# Interview Project

This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.15.1.

## Build & development

Run `grunt` for building and `grunt serve` for preview.

## Introduction

For the purpose of this test, you will be asked to, using this boilerplate Angular application, to create an application which authenticates with an API, which returns an authentication token. That authentiation token will then be used in all future API calls.

After that, the application will fetch a list of events with another API call. It will render these events to the view as a list.

Each event is clickable and if clicked, it will switch to a view showing the event details above and and a list of attendees below.

When in this view, if you click on the attendee, you will switch to another view with attendee details.



## API documentation:

Below documents the API, however, if you are familiar with Postman ( [https://www.getpostman.com/](https://www.getpostman.com/) ), you will find a collection of these API endpoints to import in the following location in the codebase: `docs/postman_collection.json`


### Authenticate:

```
[POST] http://full.im.phulse.com:8000/authenticate
```

Form data:

* username
* password

Returns:

* status
* authToken

Note: For this test, username is 'foo' and password is 'bar'


### Get events:


```
[GET] http://full.im.phulse.com:8000/events
```

Header:

* auth_token

Returns a list of:

* id
* organization
* logo
* title
* description
* start_time
* end_time
* location


### Get List of Attendees for an Event Data by ID:



```
[GET] http://full.im.phulse.com:8000/event/(event_id)/attendees
```

Example: http://full.im.phulse.com:8000/event/1/attendees

Header:

* auth_token

Query parameter:

* id

Returns a list of:

* id
* avatar
* name
* title
* profile


## Mock Wireframe 

Please note: This wireframe is purely for reference alone. Feel free to be creative!

![wireframe](http://i.imgur.com/fByYnFb.png "wireframe")


