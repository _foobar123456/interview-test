'use strict';

/**
 * @ngdoc function
 * @name interviewProjectApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the interviewProjectApp
 */
angular.module('interviewProjectApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
