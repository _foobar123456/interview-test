'use strict';

/**
 * @ngdoc function
 * @name interviewProjectApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the interviewProjectApp
 */
angular.module('interviewProjectApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
